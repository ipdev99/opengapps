# OpenGApps overlays

These overlays are required since SDK28 to support some of the Google features like setting the default Dialer or making PixelLauncher work as it dose on Pixel devices.

## Contents

This folder contains both overlay APKs and their sources in the respective folders. You're free to browse them to see what's inside each of the overlay.

## Creation process

The process is pretty basic and includes these 5 steps:

1. Set up the folder structure with the required resources
2. Set up the `AndroidManifest.xml` to target proper Android version
3. Build the overlay's APK file by calling `aapt`:

    ```shell
    aapt p -M AndroidManifest.xml -S res -I $ANDROID_SDK_ROOT/platforms/android-VERSION/android.jar -F overlay.apk.un
    ```

    where:
    - `VERSION` is the targeted Android version number (e.g. `30`)
    - `.un` suffix stands for **unaligned** (we will zipalign it later)
4. Sign the unaligned APK: `jarsigner -keystore PATH/TO/YOUR/keystore.jks overlay.apk.un YOUR_KEY_NAME`

    You'll be asked for the keystore password and the key password, if required
5. Zipalign the APK: `zipalign 4 overlay.apk.un overlay.apk`
